import React, {useEffect, useState} from "react"
import { NavLink } from "react-router-dom";

function VehicleList({ vehicles, getVehicles}) {
  const deleteVehicle = async (id) => {
    fetch(`http://localhost:8100/api/vehicles/${id}/`, {
      method:"delete",
    })
    .then(() => {
      return getVehicles()
    })
  }
  if (vehicles === undefined) {
    return null
  }
  return (
  <>
    <table className="table table-success table-striped table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Picture</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {vehicles.map((vehicle) => {
          return (
            <tr key={vehicle.id}>
              <td>{vehicle.name}</td>
              <td>{vehicle.picture_url}</td>
              <td>{vehicle.manufacturer}</td>
              <td>
                  <button type="button" value={vehicle.id} onClick={() => deleteVehicle(vehicle.id)}>Delete Vehicle</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}
export default VehicleList;
