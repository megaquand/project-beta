import React, { useState, useEffect } from 'react';

function ManufacturerCreate() {
    const [name, setName] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name

        const ManufacturerURL = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(ManufacturerURL, fetchConfig)
        if (response.ok) {
            const newManufacturer = await response.json()
            console.log(newManufacturer)
            setName('')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
            <h2>Create Manufacturer</h2>
            <form onSubmit={handleSubmit} id="create-tech-form">
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Name" className="form-control" id="name" name="name" value={name} onChange={handleNameChange} />
                    <label htmlFor="name">Name</label>
                </div>
                <button type="submit" className="btn btn-primary">Add Manufacturer</button>
            </form>
            </div>
            </div>
        </div>
    );
}

export default ManufacturerCreate;
