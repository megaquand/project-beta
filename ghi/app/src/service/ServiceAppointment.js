import React, {useState, useEffect} from 'react'

function ServiceAppointment() {
    const [customer, setCustomer] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [vin, setVin] = useState('');
    const [reason, setReason] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const technicianUrl = "http://localhost:8080/api/technicians/";

        const response = await fetch(technicianUrl);
        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateTimeChange = (event) => {
        setDateTime(event.target.value);
    };

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.vin = vin
        data.customer = customer
        data.date_time = dateTime
        data.reason = reason
        data.technician = technician

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
            setVin('')
            setCustomer('')
            setDateTime('')
            setReason('')
            setTechnician('')
    }

    useEffect(() => {
        fetchData();
    }, [])

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Add an appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                            <input required placeholder="customer" type="text" name="customer" id="customer" className="form-control" value={customer} onChange={handleCustomerChange}/>
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required placeholder="date" type="datetime-local" name="date" id="date" className="form-control" value={dateTime} onChange={handleDateTimeChange}/>
                            <label htmlFor="date"></label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required placeholder="vin" type="text" name="vin" id="vin" className="form-control" value={vin} onChange={handleVinChange}/>
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required placeholder="reason" type="text" name="reason" id="reason" className="form-control" value={reason} onChange={handleReasonChange}/>
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="mb-3">
                            <select required name="technician" id="technician" className="form-select" value={technician.id} onChange={handleTechnicianChange}>
                                <option value="">Select a technician</option>
                                {technicians.map(technician => {
                                    return(
                                        <option key={technician.id} value={technician.id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default ServiceAppointment
