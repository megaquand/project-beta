import React, { useState, useEffect } from "react";

function SalespersonAddForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const [formSubmitted, setFormSubmitted] = useState(false);

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };
  const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      const newSalesperson = response.json();
      setFirstName("");
      setLastName("");
      setEmployeeId("");
      setFormSubmitted(true);
    }
  };
  useEffect(() => {}, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                value={firstName}
                placeholder="first_name"
                required
                type="text"
                first_name="first_name"
                id="first_name"
                className="form-control"
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                value={lastName}
                placeholder="last_name"
                required
                type="text"
                last_name="last_name"
                id="last_name"
                className="form-control"
              />
              <label htmlFor="name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeIdChange}
                value={employeeId}
                placeholder="employee_id"
                required
                type="text"
                employee_id="employee_id"
                id="employee_id"
                className="form-control"
              />
              <label htmlFor="employee_id">Emploeee ID</label>
            </div>
            <button className="btn btn-success">Create</button>
          </form>
          New Salesperson Added
        </div>
      </div>
    </div>
  );
}
export default SalespersonAddForm;
