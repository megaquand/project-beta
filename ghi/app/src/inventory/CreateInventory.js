import React, { useState, useEffect } from 'react';

function CreateInventory() {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')
    const [models, setModels] = useState([])

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const fetchModels = async () => {
        const model = "http://localhost:8100/api/models/"
        const response = await fetch(model)
        if (response.ok) {
            const modelData = await response.json()
            setModels(modelData.models)
        }
    }

    useEffect(() => {
        fetchModels()
    }, [])



    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model
        const invURL = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(invURL, fetchConfig)
        if (response.ok) {
            const newInv = await response.json()
            setColor('')
            setYear('')
            setVin('')
            setModel('')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Add car to inventory</h2>
                    <form onSubmit={handleSubmit} id="create-tech-form">
                        <div className="form-floating mb-3">
                            <input required type="text" placeholder="Color" className="form-control" id="color" name="color" value={color} onChange={handleColorChange} />
                            <label htmlFor="vin">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required type="text" placeholder="VIN" className="form-control" id="vin" name="vin" value={vin} onChange={handleVinChange} />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required type="number" placeholder="Year" className="form-control" id="year" name="year" value={year} onChange={handleYearChange} />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="mb-3">
                            <select required name="model" id="model" className="form-select" value={model} onChange={handleModelChange}>
                                <option value="">Select Model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateInventory;
