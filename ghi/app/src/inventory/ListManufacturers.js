import React, {useEffect, useState } from "react";

function ListManufacturers() {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        const fetchManufacturers = async () => {
            try {
                const response = await fetch ('http://localhost:8100/api/manufacturers/');
                if (!response.ok) {
                    throw new Error('Failed to fetch manufacturers');
                }
                const data = await response.json();
                setManufacturers(data.manufacturers);
            } catch (error) {
                console.error(error);
            }
        };
        fetchManufacturers();
    }, []);

    return (
        <div>
            <h2>Manufacturers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer Name</th>
                        <th>Manufacturer ID</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manu => (
                        <tr key={manu.id}>
                            <td>{manu.name}</td>
                            <td>{manu.id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );

}

export default ListManufacturers
