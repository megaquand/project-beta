from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vip",
        "customer",
        "technician",
        "vin",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def technician_list(request): # validate the input
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder)

    else:
        try:
            data = json.loads(request.body)
            technician = Technician.objects.create(**data)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"technician": "Failed to create a technician, check details and try again. (Needs first_name, last_name, employee_id)"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def technician_detail(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
            )
    elif request.method == "PUT":
        data = json.loads(request.body)
        try:
            technician = Technician.objects.filter(id=id)
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
        technician.update(**data)
        return JsonResponse(
                Technician.objects.get(id=id),
                encoder=TechnicianEncoder,
                safe=False,
            )
    else:
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def appointment_list(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentDetailEncoder,
            status=200
        )

    else:
        try:
            data = json.loads(request.body)
            print(data)
            try:

                technician_id = data["technician"]
                technician = Technician.objects.get(id=technician_id)
                data["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Technician"},
                    status=400,
                )
            for car in AutomobileVO.objects.filter(sold=True):
                if car.vin == data["vin"]:
                    data["vip"] = True

            appointment = Appointment.objects.create(**data)
            appointment.status = "scheduled"
            appointment.save()
            return JsonResponse(
                {"appointments": appointment},
                encoder=AppointmentDetailEncoder,
                safe=False,
                status=200)
        except:
            response = JsonResponse(
                {"appointments": "Failed to make appointment, please check data and try again."}
            )
            response.status = 400
            return response


@require_http_methods(["GET", "DELETE"])
def appointment_detail(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse(
                {"message": "Appointment has been deleted"}
            )
        else:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


def appointment_cancel(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            if appointment.status == "canceled":
                return JsonResponse({"message": "Appointment has already been canceled. Make sure you're cancelling the correct appointment"})
            else:
                appointment.status = "canceled"
                appointment.save()
                response = JsonResponse({"status": "canceled"})
                response.status = 200
                return response
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


def appointment_finish(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            if appointment.status == "finished":
                return JsonResponse({"message": "Appointment has already been finished. Make sure you're finishing the correct appointment"})
            else:
                appointment.status = "finished"
                appointment.save()
                return JsonResponse({"status": "finished"})
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
